package za.co.synthesis.recognition;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.*;
import org.junit.jupiter.api.Test;

public class RecognitionUsageTest {
    @Test
    public void LoginAndFindSimilarSkills() {
        assertEquals(true, true);
    }

    @Test
    public void FindColleagueAndUpvoteTheirAttributes() {
        // voter logs in
        RecognitionSystem recognition = new RecognitionSystem();
        recognition.login("Chisha", "password");
        // assume colleagues are already created
        // Colleagues are displayed
        List<Colleague> colleagueList = recognition.getColleagues();
        // select the desired colleague and upvote their attributes
        Colleague colleague = colleagueList.get(0);
        colleague.upVoteHumilityVoteCount(1);
        colleague.upVoteLeadership(1);
        // save changed attribute values
        recognition.updateColleague(colleague);
        // view updated attributes
        colleagueList = recognition.getColleagues();
        colleague = colleagueList.get(0);
        Colleague expectedColleague = new Colleague();
        expectedColleague.upVoteHumilityVoteCount(1);
        expectedColleague.upVoteLeadership(1);
        assertEquals(expectedColleague, colleague);

    }
    @Test
    public void FindColleagueAndUpvoteTheirAttributesAndCreatingAColleague() {
        // voter logs in
        RecognitionSystem recognition = new RecognitionSystem();
        recognition.login("Chisha", "password");
        // assume colleagues are already created
        // Colleagues are displayed
        List<Colleague> colleagueList = recognition.getColleagues();
        Colleague colleague1 = new Colleague();
        recognition.addColleague(colleague1);
        // select the desired colleague and upvote their attributes
        Colleague colleague = colleagueList.get(0);
        colleague.upVoteHumilityVoteCount(1);
        colleague.upVoteLeadership(1);
        // save changed attribute values
        recognition.updateColleague(colleague);
        // view updated attributes
        colleagueList = recognition.getColleagues();
        colleague = colleagueList.get(0);
        Colleague expectedColleague = new Colleague();
        expectedColleague.upVoteHumilityVoteCount(1);
        expectedColleague.upVoteLeadership(1);
        assertEquals(expectedColleague, colleague);

    }

    @Test
    public void matchCollguesBasedOnSkillsAndInterest() {
        // assume the following three colleagues already exist in the recognition system
        RecognitionSystem recognition = new RecognitionSystem();
        Colleague colleague1 = new Colleague();
        colleague1.addSkill("Data Analysis");
        colleague1.addSkill("Machine learning");
        colleague1.addSkill("Angular");
        colleague1.addSkill("React");
        recognition.addColleague(colleague1);

        // Check specific interest listed on one colleague card
        Colleague colleague2 = new Colleague();
        colleague2.addSkill("Blockchain");
        colleague2.addSkill("Cloud");
        colleague2.addSkill("Cybersec");
        colleague2.addSkill("Python");
        recognition.addColleague(colleague2);

        Colleague colleague22 = new Colleague();
        colleague22.addSkill("Blockchain");
        colleague22.addSkill("Cloud");
        colleague22.addSkill("Cybersec");
        colleague22.addSkill("Python");
        recognition.addColleague(colleague22);

        Colleague colleague3 = new Colleague();
        colleague3.addInterest("Blockchain");
        colleague3.addInterest("Cloud");
        colleague3.addInterest("Cybersec");
        colleague3.addInterest("Python");
        recognition.addColleague(colleague3);

        // check for commonality based on skills for third colleague
        List<Colleague> similarColleagues = recognition.checkCommonality(colleague3);
        // assert that similar colleagues has colleague2 only because colleague1 does
        // not match any of colleague3 interests
        assertEquals(2, similarColleagues.size());
        assertEquals(colleague22, similarColleagues.get(1));

    }

}