package za.co.synthesis.recognition;

import java.util.*;

public class Colleague {
    private int humble;
    private int leadership;
    private ArrayList<String> interests;
    private ArrayList<String> skills;

    public Colleague() {
        this.interests = new ArrayList<String>();
        this.skills = new ArrayList<String>();
    }

    public void upVoteHumilityVoteCount(int humble) {
        this.humble = humble;
    }

    public void upVoteLeadership(int leadership) {
        this.leadership = leadership;
    }

    public int getHumble() {
        return humble;
    }

    public int getLeadership() {
        return leadership;
    }

    public void addInterest(String interest) {
        this.interests.add(interest);
    }

    public ArrayList<String> getInterests() {
        return interests;
    }

    public void addSkill(String skill) {
        this.skills.add(skill);
    }

    public ArrayList<String> getSkills() {
        return skills;
    }
    // public boolean checkCommonality(skills)

}
