package za.co.synthesis.recognition;

import java.util.*;
/**
 * A platform that allows colleagues to easily give kudos to one another.
 */
public class RecognitionSystem {
    private List<Colleague> colleagues;

    public RecognitionSystem() {
        this.colleagues = new ArrayList<Colleague>();

    }

    public void login(String username, String password) {

    }

    public List<Colleague> getColleagues() {
        return this.colleagues;
    }

    public void updateColleague(Colleague colleague) {

    }

    public void addColleague(Colleague colleague) {
        this.colleagues.add(colleague);
    }

    public List<Colleague> checkCommonality(Colleague colleague3) {
        ArrayList<Colleague> matchedColleagues = new ArrayList<Colleague>();
        for (String interest : colleague3.getInterests()) {
            for (Colleague colleague : this.colleagues) {
                if (colleague.getSkills().contains(interest) && !matchedColleagues.contains(colleague)) {
                    matchedColleagues.add(colleague);
                }
            }
        }
        return matchedColleagues;
    }
}
