import React from 'react';
import './App.css';
import Cards from'./Components/Cards/Cards';
import {Button, Row, Col, Container, Header, Form, Navbar,Nav, FormControl} from 'react-bootstrap';
import {useDispatch} from 'react-redux';
import {getCards} from './actions/cards';
import { useEffect , useState} from 'react';
import picture from './Components/images/IMG1.jpg';
import picture2 from './Components/images/Luke Head Shot.jpg';
import picture3 from './Components/images/me again.jpg';
import picture4 from './Components/images/Drikus1.jpg';
import picture5 from './Components/images/Mokgali.jpg';
import picture6 from './Components/images/JenielJ.jpg';
import picture7 from './Components/images/Jono.png';
import picture8 from './Components/images/Jason.jpg';
import picture9 from './Components/images/Kayleigh.png';
import picture10 from './Components/images/Thomas.jpeg';
import picture11 from './Components/images/Humaira.jpg';
import picture12 from './Components/images/Khondwani2.jpg';


const App =()=> {
  

  const dispatch = useDispatch();
  useEffect(()=>{
    dispatch(getCards());
  }, [dispatch]);

  function matchSkills(){
    let skills1 = ["cloud"]
    let skills2 = ["cloud", "databse"]
    let found = false
    skills1.forEach((value )=> {
      if (skills2.indexOf (value) > -1)
        found=true
    }) 
    return found
  }
  function Notify(found){
    if (found == true){
    }return "You have two matches!"}
  
  return(
    
    <Container>
          <Navbar className='navbar' className='form-group' bg="primary"
           expand='lg'>
            <Navbar.Brand className='navbar-brand' href="#edf2fb">REWARD AND RECOGNITION</Navbar.Brand>
            <Nav
              className="me-auto my-2 my-lg-5"
              style={{ maxHeight: '100px' }}
              navbarScroll>
            </Nav>
            <Form className="d-flex">
            <FormControl className='form-control'
              type="search"
              placeholder="Search"
              className="me-2"
              aria-label="Search"
            />
            <Button variant="outline-info">Search</Button>
          </Form>
          </Navbar>
          
  <Row className='form-group-cards' > 
  <Row>{Notify}</Row>
      <Col>
        <Cards 
           name='MOKGALI'
           surname='JUBANI'
           movie='Con Air'
           meal="Definitely Barbeque Ribs-- Not sure that counts as meal"
           image={picture5}
           joke=" Not sure I really get them hey. I don't have one"

        />
      </Col>
      <Col>
        <Cards
        name='LUKE'
        surname='MACHOWSKI'
        movie='Pulp Fiction'
        meal='Lasagne'
        image={picture2}
        joke="What do you call Batman and Robin after they get squashed by a steam-roller? Flatman and Ribbon"
        />
      </Col>
      <Col>
        <Cards
         name='JENIEL'
         surname='JINABHAI'
         movie='Interstellar'
         meal="Nandos"
        image={picture6}
         joke="I made a playlist for hiking, it has music from the Peanuts, The Cranberries and Eminem. I call it my trail mix"
       />
      </Col>
  </Row>
  <Row className='form-group-cards'>
    <Col>
        <Cards
        name='DRIKUS'
        surname='VAN DE WALT'
        movie='Barbie In The 12 Dancing Princesses '
        meal="Pizza because they're as cheesy as my jokes."
        image={picture4}
        joke="What do clouds wear? Thunderwear"
        skills="Cloud Computing, AWS, Cybersecurity, Java"
        interests="Artificial Intelligence, Bash, Conflict Resolution, Data Science"
        />
    </Col>
    <Col >
     <Cards
      name='JONATHAN'
      surname='JACOBS'
      movie='Inside Man '
      meal="Anything with a lot of chillies"
      image={picture7}
      joke="I just bought Old MacDonald's farm...Now I'm the C-I-E-I-O"
      skills= "Conflict Resolution, Data Science, Robotics, Public Speaking"
      interests=" Java, Artificial Intelligence, Extended Reality, Blockchain"
      
        />
    </Col>
    <Col >
      <Cards
          name='RAMTIN'
          surname='MESGARI'
          movie='Dr. Strangelove, or: How I Learned to Stop Worrying and Love the Bomb'
          meal='Spaghetti alla Nerano'
          image={picture3}
          joke="Yesterday, my brother asked, ''Can I have a bookmark?''. It`s been 23 years, and he still doesn't know my name is Ramtin."
          skills=" Java, Bash, A bit of UX, React"
          interests="Graphic Design, Cryptography, Web3, Flutter"
       
       />
    </Col>
    </Row>
    <Row className='form-group-cards'>
    <Col>
        <Cards
              name='CHISHA'
              surname='SITAMULAHO'
              image={picture}
              movie='Hidden Figures'
              meal='Nshima(Pap) with Delele(okra) and chicken'
              joke=""
              skills=""
        
        />
    </Col>
    <Col >
     <Cards
        name='JASON'
        surname='MERVIS'
        movie='Shawshank Redemption'
        meal="Sushi"
        image={picture8}
        joke="Two muffins are in an oven. The one muffin says ''Wow it's getting hot in here'', the other muffin says ''Oh my word a talking muffin''"
        />
    </Col>
    <Col >
      <Cards
       name='KAYLEIGH'
       surname='SLOGROVE'
       movie='10 Things I hate About You'
       meal="Nachos"
       joke="What do a tick and the Eiffel Tower have in common? They're both Paris sites."
       image={picture9}
       />
    </Col>
    </Row>
    <Row className='form-group-cards'>
    <Col >
      <Cards
       name='THOMAS'
       surname='MAKINS'
       movie='Interstellar'
       meal="Lasagna"
       joke="It really takes guts to be an organ donor"
       image={picture10}
      />
    </Col>
    <Col >
      <Cards
       name='HUMAIRA'
       surname='AHMED'
       movie='Little Italy'
       meal="Pasta"
       joke="What's grey and not heavy? Light grey"
       image={picture11}
       />
    </Col>
    <Col >
      <Cards
       name='KHONDWANI'
       surname='SIKASOTE'
       movie='I Am Legend'
       meal="Chicken Korma"
       joke="''What did the zero say to the eight?'' ''That belt looks good on you.''"
       image={picture12}
       />
    </Col>
    </Row>
    
    
</Container>
);
}

export default App;
