import { render } from '@testing-library/react';
import React,{ useState } from "react";
import { Card, Col, Row} from 'react-bootstrap';
import './FrontCard.css';



const Front=({image,name,surname,}) => {
    return(
    <Row xs={1} md={2} className="g-4">
        <Col>
            <Card className='Card' style={{ width: '18rem'}}>
              <Card.Img className='Card-Img' variant="top" src={image} />
                <Card.Body className='body' style={{ height: '8rem'}}>
                <Card.Title className='Title'>{name} {surname}</Card.Title>
                </Card.Body>
            </Card>
        </Col>
    </Row> 
    
    
);}
    export default Front

