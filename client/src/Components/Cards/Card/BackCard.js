import { render } from '@testing-library/react';
import React,{ useState } from 'react';
import {Button, Row, Col, Container, Card, ListGroup,ListGroupItem} from 'react-bootstrap';
import './BackCard.css';
import ThumbUpIcon from '@material-ui/icons/ThumbUpAlt';
import Popup from './Popup';


//console.log(onMatch())

const Back =({movie,meal,joke,interests, skills}) => {
    const [buttonPopup, setButtonPopup] = useState(false);

        const [count, setCount]=useState([{'category':'HUMILITY','count':0},{'category':'SKILLFUL','count':0},{'category':'LEADERSHIP','count':0},
        {'category':'HUNGRY','count':0},{'category':'SMART','count':0},{'category':'CAPABLE','count':0}])
        

        function incrementCount(index){
            let temp=[...count];
            temp[index].count += 1;
            console.log(temp)
            setCount(temp)
        }
        
        return(
            <Row xs={1} md={2} className="g-4">
            <Col>
            <Card  style={{ width: '20rem' }}>
            
            <ListGroupItem className='list-group-flush1'>
                {count.map((value, index)=>{
                   return ( <Row key={index}> <Col xs={5}> {value.category}</Col><Col > {value.count}</Col>
                    <Col xs={4}> <Button onClick= {()=>incrementCount(index)}>
                           <ThumbUpIcon frontsize="small"/>
                           <i className =''></i></Button></Col>
                           </Row>)
                })}</ListGroupItem >
        
        <div>
            
        <ListGroup className='list-group-flush2'>
        <ListGroupItem >
                <h5 class='card-title-back'>INTERESTS </h5 >
               <p className='card-text-back'>{interests}</p></ListGroupItem>
               <ListGroupItem>
                <h5 class='card-title-back'>SKILLS </h5 >
               <p className='card-text-back'>{skills}</p></ListGroupItem>
            
            
            <ListGroupItem>
                <h5 class='card-title-back'>Favorite Movie? </h5 >
               <p className='card-text-back'>{movie}</p></ListGroupItem>
            
                <ListGroupItem>
                <h5 className='card-title-back'>Favorite Meal? </h5>
                <p className='card-text-back'>{meal}</p></ListGroupItem>
    
                <ListGroupItem>
                <h5 className='card-title-back'> Best Dad Joke?</h5>
                <p className='card-text-back'>{joke}</p></ListGroupItem>
            </ListGroup>
        
         </div>
         <Button onClick={()=> setButtonPopup(true)}>Match</Button>
         <Popup trigger={buttonPopup} setTrigger={setButtonPopup}>
             <h6 className="neonText">YOU HAVE MATCHED WITH TWO PEOPLE!</h6>
         </Popup>
         </Card>
         </Col>
         </Row>
           
    );
    }


export default Back 
