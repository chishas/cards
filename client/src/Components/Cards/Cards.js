import { render } from '@testing-library/react';
import React,{ useState } from "react";
import ReactCardFlip from 'react-card-flip';
import ReactDOM  from 'react-dom';
import {Button, Row, Col, Container} from 'react-bootstrap';
import './Cards.css'
import Front from './Card/FrontCard';
import Back from './Card/BackCard';


const Cards = ({name,surname,image,movie,meal,joke,about,skills, interests}) => {
    
    const [isFlipped, setIsFlipped] = useState(false);
    const handleClick = () => {
        setIsFlipped(!isFlipped);
        };
return(
    <ReactCardFlip isFlipped={isFlipped} flipDirection="horizontal">
        <div className='CardFront'>
             <Front name={name} surname={surname} image={image} />           
        <div >{<Button className='front-button' onClick={handleClick}>Recognize</Button>} </div>
     
        </div>
         <div className='Cardback'>
            <Back movie={movie} meal={meal} 
            joke={joke} about={about} skills={skills} interests={interests}/> 
            <div>{<Button className= 'front-button' onClick={handleClick}>BACK</Button>} <Button className ="message-button">MESSAGE</Button></div>
        </div>
        </ReactCardFlip>         
) 

}

export default Cards 



