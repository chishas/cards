//import recognize from recognitionService;
//import {recognize} from './recognitionService.js';
//const recognize = require("./recognitionService.js")

describe ("Usage Tests",()=>{

  test('Identify an existing candidate and update their attributes', () => {
    //voter logs in and candidates are displayed
     let candidates=recognize.getAllCandidate()
    //From candidates displayed, voter selects the candidate they're interested in 
    let interestingCandidate=candidates[0];
    let cand=recognize.getAllCandidate("Chisha")
    //selectCandidate()
    //upvote candidate attribute . This means update candidate details
    interestingCandidate.qualities.humble="2";
    interestingCandidate.qualities.leadership="1";
 
    recognize.updateCandidate(interestingCandidate);
 
    let expectedCandidate={'name':"Chisha",
    'surname':"Sitamulaho", 
    'meal':"sushi",
    'movie':"Frozen",
   'qualities':{
       'humble':'2',
       'leadership':'1'
   }}
   
    //confirm that the user's upvote was successful 
    let actualCandidate=recognize.getAllCandidate(interestingCandidate);
 
    expect(expectedCandidate).toEqual(actualCandidate)
 
 
   }); 
   test('Add new candidate card', () => {
     //new candidate logs in and fills in card form 
      createNewCandidate()
     //display candidate card to confirm creation
     selectCandidate()
    });



})
