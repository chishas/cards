import React, {useState} from 'react';
import {TextField} from '@material-ui/core';
import {Button} from '@material-ui/core';
import {Typography} from '@material-ui/core';
import {Paper} from '@material-ui/core';
import FileBase from 'react-file-base64';


import useStyles from './styles';


const CardTemplate = () =>{
    const [postData, setPostData] = useState({creator:'', title: '', message:'', tags:'',selectedFile:''});
    const classes = useStyles();
    const handleSubmit=() =>{
    }
    const clear=()=>{

    }

    return(
        <Paper className={classes.paper}>
            <form autoComplete="off" noValidate className={`${classes.root} ${classes.form}`} onSubmit={handleSubmit}>
                <Typography variant = "h6">
                    Creating a Card
                </Typography>
                <TextField name="Creator" variant="Creator"  label = "Creator" fullWidth value={postData.creator} onChange={(e) => setPostData ({...postData, creator: e.target.value})}/>
                 <TextField name="Title" variant="Title"  label = "Title" fullWidth value={postData.title} onChange={(e) => setPostData ({...postData, Title: e.target.value})}/>
                 <TextField name="Message" variant="Message"  label = "Message" fullWidth value={postData.message} onChange={(e) => setPostData ({...postData, message: e.target.value})}/>
                 <TextField name="Tags" variant="Tags"  label = "Tags" fullWidth value={postData.tags} onChange={(e) => setPostData ({...postData, tags: e.target.value})}/>
                <div className={classes.fileInput}> <FileBase type="file" multiple={false} onDone={({base64}) => setPostData({...postData,selectedFile: base64 })}/></div>
                 <Button className={classes.buttonSubmit} variant="contained" color="primary" size="large" type="submit" fullWidth>Submit</Button>
                 <Button variant="contained" color="secondary" onClick={clear} type="" fullWidth>Clear</Button>
            </form>

        </Paper>
    )
}
export default CardTemplate;