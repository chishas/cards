import express from 'express';
import {getCards, createCard, likeCard} from '../controllers/cards.js'

const router = express.Router();

router.get('/', getCards);
router.post('/', createCard); 
router.patch('/:id/likeCard', likeCard)
export default router;