import express from 'express';
import mongoose from 'mongoose';
import CardMessage from '../models/cardMessage.js';

const router =express.Router;

export const getCards=async (req, res) => {
    try{
        const cardMessages = await CardMessage.find();
        
        res.status(200).json(cardMessages);
    }
    catch(error){
        res.status(404).json({message: error.message});
    }

}
export const createCard = async(req, res) =>{
    const body = req.body;
    const newCard = new CardMessage(card);
    try{
        await newCard.save();

        res.status(201).json(newCard);

    }
    catch(error){
        res.status(409).json({message: error.message});
    }
    res.send('Card Creation');  

}
export const likeCard = async (req, res) => {
    const { id } = req.params;

    if (!mongoose.Types.ObjectId.isValid(id)) return res.status(404).send(`No card with id: ${id}`);
    
    const card = await CardMessage.findById(id);

    const updatedCard = await CardMessage.findByIdAndUpdate(id, { likeCount: card.likeCount + 1 }, { new: true });
    
    res.json(updatedCard);
}


export default router