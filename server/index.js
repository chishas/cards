import express from 'express';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import cors from'cors';
import cardRoutes from './routes/cards.js';

const app = express();

app.use(bodyParser.json({ limit: "30mb", extended: true}));
app.use(bodyParser.urlencoded({ limit: "30mb", extended: true}));
app.use(cors());

app.use('/cards', cardRoutes);

const CONNECTION_URL = 'mongodb+srv://new-user-97:new-user-9777@cluster0.tza5i.mongodb.net/myFirstDatabase?retryWrites=true&w=majority';
const PORT= process.env.PORT ||5000;

mongoose.connect(CONNECTION_URL, { useNewUrlParser: true, useUnifiedTopology: true})
    .then(()=> app.listen(PORT, ()=> console.log(`Server running on port: ${PORT}`)))
    .catch((error)=> console.log(error.message));

